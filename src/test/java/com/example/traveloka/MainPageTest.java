package com.example.traveloka;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;

public class MainPageTest {
    private WebDriver driver;
    private MainPage mainPage;

    @BeforeEach
    public void setUp() {
        ChromeOptions options = new ChromeOptions();
        // Fix the issue https://github.com/SeleniumHQ/selenium/issues/11750
        options.addArguments("--remote-allow-origins=*");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://www.traveloka.com/en-id");

        mainPage = new MainPage(driver);
    }

    @AfterEach
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void clickCarRental() throws InterruptedException {
        //Select Cars Product
        WebElement carRentalMenu = driver.findElement(By.xpath("//div[text()='Car Rental']"));
        carRentalMenu.click();

        //Select Pick-up Location (e.g.: Jakarta)
        WebElement region = driver.findElement(By.cssSelector("input[placeholder='Enter city or region']"));
        region.sendKeys("Jakarta");

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
        WebElement chooseRegion = driver.findElement(By.cssSelector("div[aria-label='Jakarta']"));
        chooseRegion.click();

        //Select Pick-up Date & Time (e.g.: today+2d 09:00)
        DateFormat dayFormat = new SimpleDateFormat("d-M-yyyy");
        DateFormat monthFormat = new SimpleDateFormat("MMMMM yyyy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 2);
        String pickUpDate = dayFormat.format(cal.getTime());
        cal.add(Calendar.DATE, 3);
        String dropOffDate = dayFormat.format(cal.getTime());

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));

        WebElement inputStartDate = driver.findElement(By.xpath("//*[@data-testid='rental-search-form-date-input-start']"));
        inputStartDate.click();

        WebElement startDate = driver.findElement(By.xpath("//*[@data-testid='date-cell-"+pickUpDate+"']"));
        startDate.click();

        WebElement inputEndDate = driver.findElement(By.xpath("//*[@data-testid='rental-search-form-date-input-end']"));
        inputEndDate.click();

        //Search Car
        WebElement searchCar = driver.findElement(By.xpath("//*[@data-testid='rental-search-form-cta']"));
        searchCar.click();
        //Select Car
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div/div[5]/div[2]/div[2]/div/div/div[1]/div/div/div[3]/div[3]/div"))).click();

        //Select Provider
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div/div[5]/div/div[4]/div/div[2]/div[2]/div[1]"))).click();

        //Select Pick-up Location in “Rental Office”
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,250)");
        jse.executeScript("window.scrollBy(0,250)");

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div/div[5]/div/div[1]/div/div[3]/div[1]/div/div/div[3]/div[1]/div[2]/div"))).click();

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div/div[5]/div/div[1]/div/div[3]/div[1]/div/div/div[3]/div[2]/div/div/div[1]/div[1]/div/div[2]"))).click();

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div/div[5]/div/div[1]/div/div[3]/div[1]/div/div/div[3]/div[2]/div/div/div[1]/div[2]/div/div/div/div[1]"))).click();

        jse.executeScript("window.scrollBy(0,250)");

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div/div[5]/div/div[1]/div/div[3]/div[2]/div/div/div[5]/div[1]/div[2]/div"))).click();

    }

}
